# -*- coding: utf-8 -*-
"""
Created on Thu Feb 27 13:40:50 2020

@author: freiche

function to analyze tsv files created in track.py and functionalities to 
prepare
"""
import os
import numpy as np
import pandas as pd
import dclab
from dclab.features.emodulus import get_emodulus
from tqdm import tqdm, trange
from scipy.spatial import ConvexHull

from . import dft, ContFeatures

#: Media for which computation of viscosity is defined
KNOWN_MEDIA = ["CellCarrier", "CellCarrierB", "water"]

def get_x_outlet(path, PIX_SIZE=0.34, FPS=6000., channelpts_filename=None,
                 save=False, savename=None):
    """Compute the x-position as the position after leaving the outlet. Depends
    on folder structure in the analysis folder on the private share and the 
    presence of a channelpts.txt file for the corresponding measurement. Will
    also compute time after outlet ('t_outlet')"""
    df = pd.read_csv(path, delimiter="\t")

    if not ('x_outlet' in df.keys()) and not ('t_outlet' in df.keys()):
        #find corresponding channelpts file
        #find M number
        M_ind = path.find("_M")
        M_no = path[M_ind+1:M_ind+5]
        #find path to channelpts file
        path_ind = path.find("Data_TSV")
        folder = path[:path_ind]
    
        if not channelpts_filename:
            channelpts_filename = M_no + "_channelpts.txt"
    
        channelpts_filepath = os.path.join(folder,channelpts_filename)
    
        if not os.path.isfile(channelpts_filepath):
            print("IO Error: {0} does not exist.".format(channelpts_filepath))
            return None
        
        else:
            rect_pts = np.loadtxt(channelpts_filepath)
            detection_zone = [rect_pts[0][0], rect_pts[1][0]]
            detection_zone.sort()
    
            df['x_outlet'] = (detection_zone[0]*PIX_SIZE 
                              - np.array(df['pos_x_um']))
            
            #determine time after outlet (copied from track)
            pos_x = np.array(df['pos_x'])
            velocity = np.array(df['velocity'])
            frames = np.array(df['frame'])
            obj_nmbrs = np.array(df['object index'])
            time_outlet = np.array([np.nan]*len(frames))
    
            obj_nmbrs_uniq = np.unique(obj_nmbrs)
            
            for ii in obj_nmbrs_uniq:
                obj_ind = obj_nmbrs==ii
                pox_x_obj = pos_x[obj_ind]
                frames_obj = frames[obj_ind]
                velocity_obj = velocity[obj_ind]
                pos_x_outlet = pox_x_obj[pox_x_obj < detection_zone[0]]
                if len(pos_x_outlet)>0:
                    #find index when object is first time left channel
                    #=largest value of pos_x_outlet
                    ind_outlet = np.nonzero(pox_x_obj==max(pos_x_outlet))[0]
                    in_outlet = True
                else:
                    in_outlet = False
                    
                if in_outlet:
                    time0 = ((detection_zone[0] - pox_x_obj[ind_outlet]) 
                             * PIX_SIZE 
                             / (velocity_obj[ind_outlet]*1000)) #v in um/s
                    time_outlet[obj_ind] = ((frames_obj - 
                                             frames_obj[ind_outlet]) 
                                            / float(FPS) + time0)
            
            df['t_outlet'] = time_outlet

            if save:
                if savename==None:
                    savename=path    
                df.to_csv(savename, sep='\t', index=False, index_label=False)
                
            return df
        
    else:
        return df

def find_measurement_file(tsv_path, rtdc_root='guck_division2',
                          analysis_folder = r"Q:\Analysis\Experiments"):
    """Find the corresponding measurement file for a tsv file. Default searches
    for file on the share.
    
    parameters: 
        tsv_path: Path to tsv file
        root: *optional*, root folder of the project files. Default = 'share',
            meaning search for file on mplshare
        analysis_folder: *optional*, root folder of the analysis files, Default
            is on "\\mplshare\Private Home."
    """
    
    if rtdc_root=='share':
        rtdc_root = r"R:\guck_division\Data"
    elif rtdc_root=='guck_division2':
        rtdc_root = r"T:\Data"
    else:
        print("Unknown root experiment data folder {}. ".format(rtdc_root)
              + "Trying to load the data from mplshare")
        
    tsv_strip = tsv_path.strip(analysis_folder)
    sample_folder = tsv_strip.split("\\")[:-2]
    sample_folder = "\\".join(sample_folder)
    sample_folder = "\\".join([rtdc_root, sample_folder])
    
    tsv_file = tsv_path.split("\\")[-1]
    M_No = tsv_file.split("_")[-2]
    
    rtdc_file = os.path.join(sample_folder, M_No+"_data.rtdc")
    
    return rtdc_file

def get_Emodulus(tsv_path, x_window, viscosity = None, temperature=24.0,
                 pos_x_mode = 'pos_x_um', save=False, savename=None,
                 rtdc_root='guck_division2', 
                 analysis_folder = r"Q:\Analysis\Experiments"):
    """Calculate the Emodulus for every object based on the values in steady
    state
    
    parameters:
        tsv_path: path to tsv file
        x_window: *tuple*, (x_min,x_max) to assume steady state
        viscosity: *optional, float*, viscosity to use for the Emodulus 
            calculation. Default = None -> value will be taken from measurement
            file
        temperature: *optional, float* If no temperature value was written
            during the measurement, use this temperature instead.
        pos_x_mode: *optional, str*, Definition of pos_x for the x_window.
            Possible values: pos_x, pos_x_um, x_conv, x_outlet
        save: *optional, bool*, If True, save the new results to tsv
        savename: *optional, str* Filename where to save the results. If None,
            tsv_path will be overwritten.
        root: *optional*, root folder of the project files. Default = 'share',
            meaning search for file on mplshare
        analysis_folder: *optional*, root folder of the analysis files, Default
            is on "\\mplshare\Private Home."
    """
    #Find the original measurement file
    try:
        rtdc_file = find_measurement_file(tsv_path, rtdc_root=rtdc_root,
                              analysis_folder = r"Q:\Analysis\Experiments")
    except OSError():
        raise IOError("Cannot find file {}".format(rtdc_file))
        
    ds = dclab.new_dataset(rtdc_file)
    #measurement parameters
    flow_rate = ds.config['setup']['flow rate']
    medium = ds.config['setup']['medium']
    channel_width = ds.config['setup']['channel width']
    PIX_SIZE = ds.config['imaging']['pixel size']
    
    #If Medium was set to "Other"during the measurement you need to manually 
    #set a viscosity. The get_emodulus fct will then take this value for the 
    #computation
    
    valmed = [v.lower() for v in KNOWN_MEDIA + ["CellCarrier B"]]
    medium = medium.lower()
    if medium not in valmed:
        if not viscosity:
            raise ValueError("Invalid medium: {}. Need to manually set "
                             "viscosity value".format(medium))
        else:
            medium = viscosity
        
    #read in data from tsv
    df = pd.read_csv(tsv_path, delimiter="\t")
    
    pos_x = np.array(df[pos_x_mode])
    obj_nmbrs = np.array(df['object index'])
    Emods = np.array([np.nan]*len(df))

    obj_nmbrs_uniq = np.unique(obj_nmbrs)

    for ii in tqdm(obj_nmbrs_uniq):
        obj_ind = obj_nmbrs==ii
        pox_x_obj = pos_x[obj_ind]
        #only calculate the Emodulus for x-positions where the object has
        #reached steady state
        pos_x_in_window = (pox_x_obj > x_window[0]) & (pox_x_obj < x_window[1])
            
        area_um = df['area_um'][obj_ind][pos_x_in_window]
        deform = df['deform'][obj_ind][pos_x_in_window]
        
        Emod = get_emodulus(area_um, deform, medium=medium,
                            channel_width=channel_width, flow_rate=flow_rate,
                            px_um=PIX_SIZE, temperature=temperature, copy=True)
        
        mean_Emod = np.mean(Emod)
        
        Emods[obj_ind] = mean_Emod
        
    df['Emodulus'] = Emods
    
    if save:
        if savename==None:
            savename = tsv_path    
        df.to_csv(savename, sep='\t', index=False, index_label=False)
        
    return df

def get_fourier_descripors(tsv_path,
                           fourier_max_order=10,
                           rtdc_root='guck_division2', 
                           analysis_folder = r"Q:\Analysis\Experiments",
                           save = False, savename=None):
    """Calculate the fourier descriptors for the contours based on Fregin et 
    al., Nat. Com. 2019 and reconstruct the contours from the even or odd 
    descriptors only. For the reconstructed contours caclulate deformation and
    inertia ratio, also for the convex hull of the reconstructed contours.
    parameters:
        tsv_path: path to tsv file
        fourier_max_order: maximum order to calculate the fourier descriptors
        root: *optional*, root folder of the project files. Default = 'share',
            meaning search for file on mplshare
        analysis_folder: *optional*, root folder of the analysis files, Default
            is on "\\mplshare\Private Home.        save: *optional, bool*, If True, save the new results to tsv
        savename: *optional, str* Filename where to save the results. If None,
            tsv_path will be overwritten.
    """
    
    #Find the original measurement file
    try:
        rtdc_file = find_measurement_file(tsv_path, rtdc_root=rtdc_root,
                              analysis_folder = r"Q:\Analysis\Experiments")
    except OSError():
        raise IOError("Cannot find file {}".format(rtdc_file))

    #read in data from tsv
    df = pd.read_csv(tsv_path, delimiter="\t")
        
    ds = dclab.new_dataset(rtdc_file)
    
    #add new features to dataframe
    for order in np.arange(fourier_max_order+1):
        df['fourier_a'+str(order)] = np.nan
        df['fourier_b'+str(order)] = np.nan
    df['deform_fourier_odd'] = np.nan
    df['deform_fourier_even'] = np.nan
    df['deform_fourier_hull_odd'] = np.nan
    df['deform_fourier_hull_even'] = np.nan
    df['inert_ratio_fourier_odd'] = np.nan
    df['inert_ratio_fourier_even'] = np.nan
    df['inert_ratio_fourier_hull_odd'] = np.nan
    df['inert_ratio_fourier_hull_even'] = np.nan
        
    for ii in trange(len(df)):
        cont_ind =  df['index_unfiltered'][ii]
        cont = ds['contour'][int(cont_ind)]
        
        #perform fourier transform on contour as in Fregin et al., 2019
        ak, bk = dft.Fouriertrans_Fregin(cont, max_order=fourier_max_order)
        
        #reconstruct contour from even or odd descriptors only
        phi_odd, r_odd = dft.contour_reconstruction_Fregin_odd(ak, bk)
        phi_even, r_even = dft.contour_reconstruction_Fregin_even(ak, bk)
        x_odd, y_odd = dft.polar_backtransform(phi_odd, r_odd)
        x_even, y_even = dft.polar_backtransform(phi_even, r_even)
        
        cont_odd = np.vstack((x_odd, y_odd)).T
        cont_even = np.vstack((x_even, y_even)).T
        
        #the fourier contours are not always convex. Create convex hull
        hull_ind_odd = ConvexHull(cont_odd).vertices
        hull_odd = cont_odd[hull_ind_odd]

        hull_ind_even = ConvexHull(cont_even).vertices
        hull_even = cont_even[hull_ind_even]
        
        deform_odd = ContFeatures.deformation(cont_odd)
        deform_even = ContFeatures.deformation(cont_even)
        deform_hull_odd = ContFeatures.deformation(hull_odd)
        deform_hull_even = ContFeatures.deformation(hull_even)
        
        inert_ratio_odd = ContFeatures.inertia_ratio(cont_odd)
        inert_ratio_even = ContFeatures.inertia_ratio(cont_even)
        inert_ratio_hull_odd = ContFeatures.inertia_ratio(hull_odd)
        inert_ratio_hull_even = ContFeatures.inertia_ratio(hull_even)
        
        for order in np.arange(fourier_max_order+1):
            df['fourier_a'+str(order)][ii] = ak[order]
            df['fourier_b'+str(order)][ii] = bk[order]
        df['deform_fourier_odd'][ii] = deform_odd
        df['deform_fourier_even'][ii] = deform_even
        df['deform_fourier_hull_odd'][ii] = deform_hull_odd
        df['deform_fourier_hull_even'][ii] = deform_hull_even
        df['inert_ratio_fourier_odd'][ii] = inert_ratio_odd
        df['inert_ratio_fourier_even'][ii] = inert_ratio_even
        df['inert_ratio_fourier_hull_odd'][ii] = inert_ratio_hull_odd
        df['inert_ratio_fourier_hull_even'][ii] = inert_ratio_hull_even

    if save:
        if savename==None:
            savename = tsv_path    
        df.to_csv(savename, sep='\t', index=False, index_label=False)
        
    return df
        
