# -*- coding: utf-8 -*-
"""
Created on Wed Mar 27 22:36:31 2019

@author: s8570974
"""
import numpy as np
import os
import pandas as pd
import matplotlib.pyplot as plt
import dclab

#path to .rtdc or .tdms data file
data_path = r"C:\Users\s8570974\Documents\Experiment Data\20190327_Felix_deformation_variation_steadystate\20um_0.06MC\M001_data.rtdc"

areamin = 50
areamax = 200
aratio_max = 1.05
#
ds = dclab.new_dataset(data_path)
#
ds.config["filtering"]["area_um min"] = areamin
ds.config["filtering"]["area_um max"] = areamax
ds.config["filtering"]["area_ratio"] = aratio_max
ds.apply_filter()  # this step is important!

ds_filtered = dclab.new_dataset(ds)

#ds.config["filtering"]["deform max"] = .1
#ds.apply_filter()
dif = ds.filter.all

f, axes = plt.subplots(1, 3, sharex=True, sharey=True)
axes[0].plot(ds["area_um"], ds["bright_avg"], "o", alpha=.2)
axes[0].set_title("unfiltered")
axes[1].plot(ds["area_um"][dif], ds["bright_avg"][dif], "o", alpha=.2)
axes[1].set_title("Deformation <= 0.1")
axes[2].plot(ds_filtered["area_um"], ds_filtered["bright_avg"], "o", alpha=.2)
axes[2].set_title("child data")

for ax in axes:
    ax.set_xlabel(dclab.dfn.feature_name2label["area_um"])
    ax.set_ylabel(dclab.dfn.feature_name2label["bright_avg"])

plt.tight_layout()
plt.show()