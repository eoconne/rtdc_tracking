# -*- coding: utf-8 -*-
"""
Created on Tue Aug 20 15:21:53 2019

@author: s8570974

Functions to create a Polygon filter to filter the velocity data
"""
import sys
import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtGui

from dclab import PolygonFilter

def create_PolygonFilter(xdata, ydata, PolyInit):
    app = QtGui.QApplication([])
    w = pg.GraphicsWindow(size=(600,400), border=True)
    w.setWindowTitle('Polygon filter for velocity curve')

    v_plot = w.addPlot(title = "Set Filter for curve. DONT ADD EXTRA HANDLES!!!",
                       x=xdata, y=ydata,
                       pen=None, symbol='o', symbolBrush='b') #setting pen=None disables line drawing
    #create Polygon ROI element
    polyROI = pg.PolyLineROI(PolyInit, closed=True)
    v_plot.addItem(polyROI)

    #not really sure what this does. Taken from example docs
    v_plot.disableAutoRange('xy')
    v_plot.autoRange()

    if (sys.flags.interactive != 1) or not hasattr(QtCore, 'PYQT_VERSION'):
        QtGui.QApplication.instance().exec_()
    #blocks the execution of the rest of the code as long as plot window is
    #opened
#     if sys.flags.interactive != 1 or not hasattr(QtCore, 'PYQT_VERSION'):
#         pg.QtGui.QApplication.exec_()
        
    points_PolyFilter = np.array([np.array(handle.pos()) for handle in 
                                  polyROI.getHandles()])

    return points_PolyFilter

def PolyFilter(xdata, ydata, PolyInit, createPoly=True, returnPoly = False, **kws):
    '''Find data points that are within polygon PointPolyFilter. 
    Returns array for data slicing'''    
    if createPoly:
        points_PolyFilter = create_PolygonFilter(xdata, ydata, PolyInit)
    else:
        if not 'points_PolyFilter' in kws:
            raise ValueError("If createPoly=False the points of the Polygon must be given by"
                             +"points_PolyFilter=")
        else:
            points_PolyFilter = kws['points_PolyFilter']
    
    ind_PolyFilter = [PolygonFilter.point_in_poly(p, points_PolyFilter) 
                        for p in zip(xdata, ydata)]
    
    if returnPoly:
        return ind_PolyFilter, points_PolyFilter
    else:
        return ind_PolyFilter

def get_PolyFilterVelocity(x_positions, velocity, channel_zone,
                          inlet_offset=30, outlet_offset=10,
                          returnPoly = False, createPoly=True, **kws):
    #median velocity in channel
    v_med = np.median(velocity[(x_positions>channel_zone[0])
                        &(x_positions<channel_zone[1])])
    
    #initialize default points of the polygon based on the data
    p1 = [0,0]
    p2 = [0, min(velocity)]
    p3 = [.5 * channel_zone[0], 0.55 * v_med]
    p4 = [channel_zone[0], 1.1 * v_med]
    p5 = [channel_zone[1]-inlet_offset, 1.1 * v_med]
    p6 = [(max(x_positions)-channel_zone[1])/2.+channel_zone[1], 0.55 * v_med]
    p7 = [max(x_positions), min(velocity)]
    p8 = [max(x_positions), 0]
    p9 = [(max(x_positions)-channel_zone[1])/2.+channel_zone[1], 0.45 * v_med]
    p10 = [channel_zone[1]-inlet_offset, .9 * v_med]
    p11 = [channel_zone[0], .9 * v_med]
    p12 = [.5 * channel_zone[0], 0.45 * v_med]
    defaultpoints_PolyFilter = [p1,p2,p3,p4,p5,p6,p7,p8,p9,p10,p11,p12]

    if createPoly:
        ind_PolyFilter,points_PolyFilter = PolyFilter(x_positions, velocity, 
                                                      defaultpoints_PolyFilter,
                                                      returnPoly=True)        
    else:
        if not 'points_PolyFilter' in kws:
            raise ValueError("If createPoly=False the points of the Polygon must be given by"
                             +"points_PolyFilter=")
        else:
            points_PolyFilter = kws['points_PolyFilter']
            ind_PolyFilter, points_PolyFilter = PolyFilter(x_positions,
                                                           velocity, 
                                                           points_PolyFilter,
                                                           createPoly=False, 
                                                           returnPoly=True,
                                                           points_PolyFilter=points_PolyFilter)
    
    if returnPoly:
        return ind_PolyFilter, points_PolyFilter    
    else:
        return ind_PolyFilter
