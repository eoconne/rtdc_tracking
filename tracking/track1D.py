# -*- coding: utf-8 -*-
"""
Created on Fri Feb 15 21:04:28 2019

@author: s8570974

Functions to distinguish bw different object that where captured multiple times
"""
import numpy as np
from itertools import chain
import cv2
import os
from matplotlib.pyplot import cm
from imageio import get_writer#, get_reader

### Object tracking ###
def med_displacement(frames, pos_x):
    
    frameNrsUniq, uniqIndex = np.unique(frames, return_index=True)
    #get average velocity from single cell events only
    pos_xUniq = np.array(pos_x)[uniqIndex]
    
    pos_x_diff = np.diff(pos_xUniq) * -1 #object moving forward results in neg value
    #throw out all negative values --> here a new object appeared
    #only take displacement of consecutive events
    consecFrames = np.diff(frameNrsUniq)[pos_x_diff > 0]
    pos_x_diff = pos_x_diff[pos_x_diff > 0]
    pos_x_diff = pos_x_diff[consecFrames == 1]
    med_x_displacement = np.median(pos_x_diff)
    
    #cut off outliers in data for more reliable value
    #data = data within 1 sigma intervall
    pos_x_diff = pos_x_diff[pos_x_diff > med_x_displacement - np.std(pos_x_diff)]
    pos_x_diff = pos_x_diff[pos_x_diff < med_x_displacement + np.std(pos_x_diff)]
    med_x_displacement = np.median(pos_x_diff)
    
    #Uncertanty for the median displacement of the objects
    Delta_x_displacement = 2*np.std(pos_x_diff)
    
    return med_x_displacement, Delta_x_displacement

def seperate_objects(DATA, WindowSize=25):
    """Assign object number to the events detecetd with GetVidData.
    
    Parameters:
        DATA: *dictionary*
            DATA dictionary from GetVidData
        WindowSize: *float*
            Minimum size of the tolerance region in um for predicting where 
            object will be in the next frame.
            
    Returns:
        out: *dictionary*
            Dictionary that contains info sorted for each object. keys are of 
            format 'Object 0', 'Object 1', ...
    """
    #pos_x[i] + (med_x_displacement +/- Delta_x_displacement) creates a zone where 
    #the obect from frame i is expected in frame i+1     
    frames = np.array(DATA['frame'])
    Index = np.array(DATA['index'])
    Pos_x = np.array(DATA['pos_x'])
    frames_uniq = np.unique(frames)        
    X_positions = []
    EventIndices = []

    med_x_displacement, Delta_x_displacement = med_displacement(frames, Pos_x)
    
    if Delta_x_displacement < WindowSize:
        Delta_x_displacement = WindowSize
                 
    #create list that contains all entries in each frame for pos_x and event_index
    for frame in frames_uniq:
        ind = np.nonzero(frames == frame)[0]
        X_positions.append(Pos_x[ind])    
        EventIndices.append(Index[ind])
    
    #dictionary to save object Data
    Objects = {}
    
    #create list that contains objects that should be kept track off 
    tracked_objects = [0]
    
    for frame_ind in range(len(frames_uniq)):
    
        if frame_ind == 0:     
            stop_tracking = []
        else:
            for remove in stop_tracking:
                tracked_objects.remove(remove)
            stop_tracking = []

        frame = frames_uniq[frame_ind]
        #Iterate over events in the frame        
        for i in range(len(EventIndices[frame_ind])):
            Ind = EventIndices[frame_ind][i]
            X = X_positions[frame_ind][i]
            
            if frame_ind == 0:
                key = "Object 0"
                Objects[key] = [(frame, Ind, X)]
            else:    
                Found = False                
                for tr_obj in tracked_objects:
                    key = "Object " + str(tr_obj)
                    prev_X =  Objects[key][-1][-1]
                    prev_frame = Objects[key][-1][0]

                    #recalculate median displacement for the object
                    if len(Objects[key]) > 3:
                        #get object path so far
                        obj_X = np.array(Objects[key])[:,2]
                        obj_frames = np.array(Objects[key])[:,0]
                        
                        #two events in same frame might be assigned to same
                        #object. Gets filtered out later
                        if len(obj_frames) == len(np.unique(obj_frames)):
                            displacement = (np.diff(obj_X) 
                                            / np.diff(obj_frames))
                            #For med_displacement don't take into account if
                            #cell got stuck at a constriction: displacement 
                            #must be larger than 1!
                            displacement = displacement[displacement > 1]
                            #recalculate median displacement
                            if len(displacement) > 5:
                                med_displacement_new = np.median(displacement)
                    
                                predZoneStart = (prev_X - (frame - prev_frame)
                                                * med_displacement_new 
                                                - Delta_x_displacement)
                                predZoneEnd = (prev_X - (frame - prev_frame) 
                                              * med_displacement_new 
                                              + Delta_x_displacement)
                                prediction_zone = (predZoneStart,predZoneEnd)
                        
                            else:
                                predZoneStart = (prev_X - (frame - prev_frame) 
                                                * med_x_displacement 
                                                - Delta_x_displacement)
                                predZoneEnd = (prev_X - (frame - prev_frame)
                                               * med_x_displacement 
                                               + Delta_x_displacement)
                                prediction_zone = (predZoneStart,predZoneEnd)

                        else:
                            predZoneStart = (prev_X - (frame - prev_frame) 
                                            * med_x_displacement 
                                            - Delta_x_displacement)
                            predZoneEnd = (prev_X - (frame - prev_frame)
                                           * med_x_displacement 
                                           + Delta_x_displacement)
                            prediction_zone = (predZoneStart,predZoneEnd)
                            
                    else:
                        predZoneStart = (prev_X - (frame - prev_frame) 
                                        * med_x_displacement 
                                        - Delta_x_displacement)
                        predZoneEnd = (prev_X - (frame - prev_frame)
                                       * med_x_displacement 
                                       + Delta_x_displacement)
                        prediction_zone = (predZoneStart,predZoneEnd)

                    #check if prediction zone is outside the channel
                    if prediction_zone[1] < 0:
                        stop_tracking.append(tr_obj)                        
                    else:                    
                        if X > prediction_zone[0] and X < prediction_zone[1]:
                            Objects[key].append((frame, Ind, X))
                            Found = True
                                                
                if Found == False:
                    new_obj_nr = len(Objects)
                    key = "Object " + str(new_obj_nr)
                    tracked_objects.append(new_obj_nr)
                    Objects[key] = [(frame, Ind, X)]
                    
                #list might contain same element twice --> removing procedure 
                #causes error --> get rid of duplicates
                stop_tracking = list(np.unique(stop_tracking))
    
    return Objects

def get_ObjectData(Objects, DATA):
    """Assign parameters to the Objects dictionary determined before.

    Parameters:
        Objects: *dictionary*
            Objects dictionary from fct seperate_objects
        DATA: *dictionary*
            DATA dictionary with video data from GetVidData
    Returns:
        out: *dictionary*, *dictionary*
            DATA, ObjectsData\n
            DATA: dict like DATA with assigned parameters for every event
            ObjectsDATA: dict where data is sorted for each object, keys are of 
            format 'Object 0', 'Object 1', ...
    """
    #Create new dictionary where parameters are assigned for every object and 
    #frame
    ObjectsData = {}
        
    object_index = np.zeros_like(DATA['frame'])
    
    for j in range(len(Objects)):
        key = 'Object ' + str(j)
        objectdata = {}
        ind = np.array(Objects[key], dtype = int)[:,1]
        
        for DataKey in DATA:
            objectdata[DataKey] = np.array(DATA[DataKey])[ind]
            
        ObjectsData[key] = objectdata
        object_index[ind] = j
    
    DATA['object index'] = object_index
    
    return DATA, ObjectsData

def filter_ObjectsDATA(ObjectsData_dict, DATA, detection_zone, min_events=3):
    """Filter the ObjectsData dictionary for unwanted events: 2 objects in same
    prediction zone; Object path not from beginning to end. Filtered objects 
    will be deleted from ObjectsData_dict.
    
    Parameters: 
        ObjectsData_dict: *dictionary*
            Dictionary containing objects data acquired with get_ObjectData().
            Filtered objects will be deleted from this dictionary.
        DATA: *dictionary*
            DATA dictionary with video data from GetVidData
    
    out: *dictionary*
        DATA_filtered
            DATA dictionary filtered for unwanted events
    """
    #filter the Objectsdata dict for bad events
    #save the keys of the bad objects in list and iterate over them later and 
    #delete
    del_keys = []
    DATA_filtered = DATA.copy()

    for key in ObjectsData_dict.keys():
        obj_nr = int(key.split(" ")[-1])
        obj_ind = np.nonzero(DATA_filtered['object index'] == obj_nr)[0]

        #if more events where written to ObjectsData then available in DATA, 
        #something went wrong while data writing 
        if (len(np.array(DATA['frame'])[obj_ind]) 
            != len(ObjectsData_dict[key]['frame'])):
            
            del_keys.append(key)
        
        #exclude two ore more objects in same pred_zone
        if (len(np.unique(ObjectsData_dict[key]['frame'])) 
                != len(ObjectsData_dict[key]['frame'])):
            
            del_keys.append(key)
            
        #measured cells must make it from channel entrance to exit
#        if (ObjectsData_dict[key]['pos_x'][0] < detection_zone[1] - 100 
#            or ObjectsData_dict[key]['pos_x'][-1] > detection_zone[0] + 100): 
#    
#            del_keys.append(key)

        #minimum number of events required per cell
        if len(ObjectsData_dict[key]['frame']) < min_events:
            del_keys.append(key)
            
    for key in set(del_keys):
        ObjectsData_dict.pop(key)
        
        obj_nr = int(key.split(" ")[-1])
        obj_ind = np.nonzero(DATA_filtered['object index'] == obj_nr)[0]
                    
        for datakey in DATA.keys():
            DATA_filtered[datakey] = np.delete(np.array(DATA_filtered[datakey]),
                                                 obj_ind)

    return DATA_filtered, ObjectsData_dict
    
def get_VelocityAndTime(ObjectsData, DATA, detection_zone, FPS, PIX_Size):
    """Add velocity and time information to the ObjectsData dictionary and DATA
    dict. For every object velocity[0] = velocity[1] and time[0] is determined 
    by velocity[0] and the distance of the object to channel entry. 
    
    Parameters:
        ObjectsData: *dictionary*
            Dictionary containing objects data acquired with get_ObjectData().
            Filtered objects will be deleted from this dictionary.
        DATA: *dictionary*
            DATA dictionary with video data from GetVidData
    
    out: *dictionary*
        DATA
            DATA dictionary with added velocity and time info
    """
    
    DATA_index = DATA['index']
    DATA['velocity'] = np.zeros(len(DATA_index))
    DATA['time'] = np.zeros(len(DATA_index))

    if 'frame_tdms' in DATA.keys():
        tdms_flag = True
    else:
        tdms_flag = False
        
    for key in ObjectsData.keys():
        
        pos_x = ObjectsData[key]['pos_x']
        
        if tdms_flag:
            frames = ObjectsData[key]['frame_tdms']
        else:
            frames = ObjectsData[key]['frame']
        
        velocity = np.zeros_like(frames)
        time = np.zeros_like(frames)
        
        Delta_frames = np.diff(frames)
        if len(Delta_frames) > 1: 

            #velocity in um/s
            velocity[1:] = abs(np.diff(pos_x)) * PIX_Size / Delta_frames * FPS 
            velocity[0] = velocity[1]
            
            time0 = (detection_zone[1] - pos_x[0]) * PIX_Size / velocity[0]
            time = (frames - frames[0]) / float(FPS) + time0
            
            #velocity in mm/s
            ObjectsData[key]['velocity'] = velocity / 1000.
            ObjectsData[key]['time'] = time
    
            #add DATA to DATA dict
            index = ObjectsData[key]['index']   
            for ii in range(len(index)):
                ind = np.nonzero(DATA_index == index[ii])[0]
                DATA['velocity'][ind] = velocity[ii] / 1000.
                DATA['time'][ind] = time[ii]  
                
    return DATA, ObjectsData

def sort_DATA_by_objNr(DATA):
    """Sort the data in the DATA dictionary so that it is arranged by objects nr
    instead of event index.

    Parameters:
        DATA: *dictionary*
            DATA dictionary with video data from GetVidData
    
    out: *dictionary*
        DATA
            DATA dictionary with added velocity and time info    
    """

    obj_nrs = DATA['object index']
    index = np.arange(len(obj_nrs))

    #get unique list of all object indices    
    obj_nrs_uniq = np.sort(list(set(obj_nrs)))
    index_new = []
    
    for ii in obj_nrs_uniq:
        ind_obj = np.nonzero(obj_nrs == ii)[0]
        #find the indeces assigned to the object nr and sort
        index_obj = np.sort(index[ind_obj])
        index_new.append(index_obj)
            
    #get 1D-array from list of lists 
    index_new = np.array(list(chain.from_iterable(index_new)))
    
    ind = np.nonzero(index == index_new)[0]
    
    #use new indexing on all data in DATA
    returnDATA = {}
    for key in DATA.keys():
        returnDATA[key] = np.array(DATA[key])[ind]
        
    return returnDATA

def DataToTSV(Dict,tsv_path):
    """Write the Data from the DATA dictionary to a .tsv file. Does not include
    contour data!
    
    Parameters:     
        Dict: *dictionary*
            Dictionary that contains the data arrays determined by 
            get_ObjectData
        tsv_path: *string*
            File path where results file will be saved.
    """
    
    header = "\t".join([key for key in Dict.keys() 
                        if not (key == 'DFT_coeffs' or key == 'contour_raw' 
                                or key == 'conv_hull' or key == 'image')])
                        
    data = [np.array(Dict[key]) for key in Dict.keys() 
            if not (key == 'DFT_coeffs' or key == 'contour_raw' 
                                        or key == 'conv_hull'
                                        or key == 'image')]

    if 'DFT_coeffs' in Dict.keys():
        #save real and imaginary part separated by column for each harmonic
        for ind, c in zip(range(len(Dict['DFT_coeffs'])),Dict['DFT_coeffs']):
            data.append(c.real)
            header += "\tDFT_coeff_real_" + str(ind) 
            data.append(c.imag)
            header += "\tDFT_coeff_imag_" + str(ind)
    
    data = np.array(data).transpose()
    
    with open(tsv_path, "w") as tfile:
        # write header
        tfile.write("# "+header+"\n")
        np.savetxt(tfile, data, delimiter = "\t", fmt = '%.8f')

###############################
### Video writing functions ###
###############################
        
def extend_img(img, macro_block_size = 16):
    """Add black pixels at right and bottom border of image, so that 
    xy-size % macro_block_size = 0
    
    Parameters:
        img: *ndarray*
            2D or 3D np.array of image pixel values
        macro_block_size: *int*
            Integer value that image.size will be made dividable to
            
    out: *ndarray*
        img + black pixels at right and bottom border
    """
    if len(img.shape) == 2:
        x_size = img[0,:].size
        y_size = img[:,0].size
    if len(img.shape) == 3:
        x_size = img[0,:,0].size
        y_size = img[:,0,0].size    
     
    if x_size % macro_block_size == 0:
        right_add = 0
    else:
        right_add = ((x_size // macro_block_size + 1) * macro_block_size
                      - x_size)

    if y_size % macro_block_size == 0:
        bottom_add = 0
    else:
        bottom_add = ((y_size // macro_block_size + 1) * macro_block_size
                      - y_size)
 
    frame = cv2.copyMakeBorder(img, 0, bottom_add, 0, right_add, 
                               borderType = cv2.BORDER_CONSTANT)
    
    return frame

def VideoWriteSepObjects(data_set, DATA, FirstFrame=0, LastFrame=0,
                         save_vidfile = "Videos\\sepCells_avi.avi",
                         macro_block_size=16):
    """Write video showing which object number was assigned to each detected 
    event.
    Parameters:
        data_set: *dclab.dataset*
            dclab dataset that was analyzed.
        DATA: *dictionary*
            Dictionary containing the object information for each videoframe
        FirstFrame: *int, optional*
            First frame in vidfile to analyze. Default is FirstFrame = 0.
        LastFrame: *int, optional*
            Last frame in vidfile to analyze. If LastFrame = 0 (default) all 
            video frames will be analyzed
        save_vidfile: *string, optional*
            File path where finished video will be saved. If not specified, the
            default path is ".\Offline\test_avi.avi" 
    """

    if save_vidfile == "Videos\\sepCells_avi.avi":
        if not os.path.isdir("Videos"):
            os.makedirs("Videos")
    
    else:
        viddir = "\\".join(save_vidfile.split("\\")[:-1])
        if not os.path.isdir(viddir):
            raise IOError("Directory \"" + viddir + "\" not found. Please " 
                          "create folder first")
            
    vidwriter = get_writer(save_vidfile, fps=60, quality = 8,
                                   macro_block_size = macro_block_size)
    
    #default colors of matplotlib. * 255 to convert to uint8 for use with cv2
    colors = np.array(cm.get_cmap('tab10').colors) * 255
    contourData = np.array(DATA['contour_raw'])
    object_index = DATA['object index']
    object_index = np.array(object_index, dtype = int)
    Pos_x = np.array(DATA['pos_x'])
    Pos_y = np.array(DATA['pos_y'])
    font = cv2.FONT_HERSHEY_SIMPLEX

    if LastFrame == 0 or LastFrame > len(data_set['frame']):
        LastFrame = len(data_set['frame'])
    if FirstFrame < 0:
        FirstFrame = 0
        
    #don't use duplicates of frames in video --> unique frames only
    frames = data_set['frame'][FirstFrame:LastFrame]
    frames_uniq, index_uniq_frames = np.unique(frames, return_index=True)

    for ii in index_uniq_frames:
        #get image from dataset
        image = data_set['image'][ii]  
        image = cv2.cvtColor(image, cv2.COLOR_GRAY2RGB)

        frame = data_set['frame'][ii]
        #if the frame number is in the filtered/tracked data: draw the contour
        if frame in DATA['frame']:
            #find index of frame in DATA
            frame_ind = np.nonzero(DATA['frame'] == frame)[0]
    
            #parameters of events found in this frame 
            contours = contourData[frame_ind]
            objects = object_index[frame_ind]
            pos_x = Pos_x[frame_ind]
            pos_y = Pos_y[frame_ind]
    
            #write video to see how detection worked out
            if len(frame_ind)>0:
                for cont in range(len(frame_ind)):
                    #get object index
                    obj_ind = objects[cont]
    
                    #get contour object in format suited for cv2.drawContours
                    #dtype=int necessary for data from tdms files
                    cont_ii = [np.array(contours[cont], dtype=int)]
                    #draw closed colored contour around object
                    cv2.drawContours(image, cont_ii, -1,
                                     colors[obj_ind % len(colors)], -1)
                    #write object index above object
                    cv2.putText(image, str(obj_ind), 
                                (int(pos_x[cont])-5,int(pos_y[cont])),
                                font, 0.5, (255,255,255),2,cv2.LINE_AA)
        
        #extend image with black pixels, so each dimension is dividable by
        #macro_block_size; needed for vid codecs to work correctly
        image = extend_img(image, macro_block_size = macro_block_size)
        vidwriter.append_data(image)

    vidwriter.close()

if __name__ == "__main__":
    import sys
    import dclab 
    sys.path.append(r'C:\Users\s8570974\py-packages\py36')
    from pyrtdc.misc import define_rect
    
#    in_path = r"E:\20190327_Felix_deformation_variation_steadystate\20um_0.05MC"
    in_path = r"E:\20180628_Felix_HL60_Dynamics\20180628_S3_20um"
    measurement = 3
    
#    mno_string = "M" + str(1000 + measurement)[1:]
#    filename = mno_string + "_data.rtdc"

    mno_string = "M" + str(measurement)
    filename = mno_string + "_data.tdms"
    
    in_file = in_path + "\\" + filename    
    tsv_name = in_path.split("\\")[-1] + "_" + mno_string + "_track_results.tsv"
    
    #set filters
    areamin = 50
    areamax = 300
    aratio_max = 1.05
    
    first_frame = 0    #must be int
    last_frame = 1000  #must be int
    
    #read in experiment file and filter
    ds = dclab.new_dataset(in_file)
    
    ds.config["filtering"]["area_um min"] = areamin
    ds.config["filtering"]["area_um max"] = areamax
    ds.config["filtering"]["area_ratio max"] = aratio_max
    ds.config["filtering"]["frame min"] = ds['frame'][first_frame] 
    ds.config["filtering"]["frame max"] = ds['frame'][last_frame]
    ds.apply_filter()  # this step is important!
    
    ds_filtered = dclab.new_dataset(ds)
    
    #get data in format to be used with tracking functions
    DATA = {}
    
    DATA["frame"] = ds_filtered["frame"]
    DATA["index"] = ds_filtered["index"] - 1 #dclab index starts at 1
    DATA["index_unfiltered"] = ds['index'][ds.filter.all] - 1
    DATA["area"] = ds_filtered["area_um"] 
    DATA["deformation"] = ds_filtered["deform"]
    DATA["area ratio"] = ds_filtered["area_ratio"]
    DATA["length"] = ds_filtered["size_x"]
    DATA["height"] = ds_filtered["size_y"]
    DATA["pos_x"] = ds_filtered["pos_x"] / 0.34 #pos_x in pixel
    DATA["pos_y"] = ds_filtered["pos_y"]
    DATA["inertia_ratio"] = ds_filtered["inert_ratio_cvx"]
    DATA["inertia_ratio_raw"] = ds_filtered["inert_ratio_raw"]
    DATA["contour_raw"] = ds_filtered["contour"]
#    DATA["conv_hull"] = ds_filtered["contour"]
    DATA['image'] = list(ds_filtered["image"])
    
    for key in DATA.keys():
        DATA[key] = list(DATA[key])
    
    if in_file.endswith('.tdms'):
        imfile = in_path + "\\" + mno_string + "_bg_102.png"
    elif in_file.endswith('.rtdc'):
        imfile = in_path + "\\" + mno_string + "_bg.png"
    
    img = cv2.imread(imfile)
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)
    img_resize = 1.
    rect_pts = define_rect(img, resize = img_resize)
    
    detection_zone = [rect_pts[0][0]/img_resize, rect_pts[1][0]/img_resize]
    detection_zone.sort()
    
    PIX_SIZE = ds_filtered.config['imaging']['pixel size']
    FPS = ds_filtered.config['imaging']['frame rate']
    
    Objects = seperate_objects(DATA)#, WindowSize=100.)
    DATA, ObjectsData = get_ObjectData(Objects, DATA)
    
    DATA, ObjectsData = get_VelocityAndTime(ObjectsData, DATA, detection_zone,
                                            FPS, PIX_SIZE)
    
    DATA_filtered, ObjectsData = filter_ObjectsDATA(ObjectsData, DATA, detection_zone,
                                       min_events=3)
    
    DATA_filtered = sort_DATA_by_objNr(DATA_filtered)
    
    DataToTSV(DATA_filtered,tsv_name)
    
    vid_path="Videos\\sepCells_avi.avi"
#    VideoWriteSepObjects(ds, DATA, FirstFrame=0, LastFrame=200,
    VideoWriteSepObjects(ds, DATA_filtered,
                         FirstFrame=first_frame, LastFrame=last_frame,
                         save_vidfile = vid_path, macro_block_size=16)